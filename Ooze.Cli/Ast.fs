namespace Ast

type keyword =
  | Break
  | Case
  | Catch
  | Class
  | Continue
  | Default
  | Do
  | Else
  | Enum
  | Export
  | Extends
  | False
  | Finally
  | For
  | Function
  | If
  | Implements
  | In
  | Interface
  | Let
  | New
  | Null
  | Package
  | Private
  | Protected
  | Public
  | Return
  | Static
  | Super
  | Swtich
  | This
  | Throw
  | True
  | Try
  | While
  | Yield

type unary_op =
  | Decrement
  | Increment
  | Neg
  | NegBits
  | Not

type binary_op =
  | OpAdd
  | OpAnd
  | OpArrow
  | OpAssign
  | OpAssignOp of binary_op
  | OpBoolAnd
  | OpBoolOr
  | OpDiv
  | OpEq
  | OpGt
  | OpGte
  | OpIn
  | OpLt
  | OpLte
  | OpMod
  | OpMul
  | OpNotEq
  | OpOr
  | OpShl
  | OpShr
  | OpSub
  | OpXor

type constant =
  | Float of string
  | Identity of string
  | Int of string
  | Regexp of string * string
  | String of string

type token =
  | Eof
  | Const of constant
  | Keyword of keyword
  | Comment of string
  | CommentLine of string
  | UnaryOp of unary_op
  | BinOp of binary_op
  | Semicolon
  | Comma
  | BraceOpen
  | BraceClose
  | ParenOpen
  | ParenClose
  | Dot
  | DoubleDot
  | Arrow
  | IntInterval of string
