﻿namespace Ooze.Cli

open Argu

[<CliPrefix(CliPrefix.DoubleDash)>]
[<NoAppSettings>]
type OozeArguments =
  | [<AltCommandLine("-m")>]                     Main   of CLASS:string
  | [<EqualsAssignment; AltCommandLine("-D")>]   Define of VAR:string * VALUE:string
  | [<AltCommandLine("-V")>]                     Verbose
  | [<AltCommandLine("-v")>]                     Version
with
  interface IArgParserTemplate with
    member arg.Usage =
      match arg with
        | Main _   -> "select startup class"
        | Define _ -> "define a conditional compilation flag"
        | Verbose  -> "turn on verbose mode"
        | Version  -> "output version information and exit"
