﻿module Ooze.Cli.Main

open System
open Argu

[<EntryPoint>]
let main argv =
  let errorHandler = ProcessExiter( colorizer = function ErrorCode.HelpText -> None | _ -> Some ConsoleColor.Red )
  let parser = ArgumentParser.Create<OozeArguments>( programName = "ooze", errorHandler = errorHandler )

  let results = parser.ParseCommandLine argv

  0
