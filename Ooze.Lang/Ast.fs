namespace Ast

type keyword =
  | Break
  | Case
  | Catch
  | Class
  | Continue
  | Default
  | Do
  | Else
  | Enum
  | False
  | Finally
  | Export
  | Extends
  | For
  | Function
  | If
  | In
  | Interface
  | Implements
  | Let
  | New
  | Null
  | Package
  | Public
  | Private
  | Protected
  | Return
  | Super
  | Static
  | Swtich
  | This
  | Throw
  | True
  | Try
  | While
  | Yield

type unary_op =
  | Increment
  | Decrement
  | Not
  | Neg
  | NegBits

type binary_op =
  | OpAdd
  | OpMul
  | OpDiv
  | OpSub
  | OpAssign
  | OpEq
  | OpNotEq
  | OpGt
  | OpGte
  | OpLt
  | OpLte
  | OpAnd
  | OpOr
  | OpXor
  | OpBoolAnd
  | OpBoolOr
  | OpShl
  | OpShr
  | OpMod
  | OpAssignOp of binary_op
  | OpArrow
  | OpIn

type constant =
  | Int of string
  | Float of string
  | Regexp of string * string
  | String of string
  | Identity of string

type token =
  | Eof
  | Const of constant
  | Keyword of keyword
  | Comment of string
  | CommentLine of string
  | UnaryOp of unary_op
  | BinOp of binary_op
  | Semicolon
  | Comma
  | BraceOpen
  | BraceClose
  | ParenOpen
  | ParenClose
  | Dot
  | DoubleDot
  | Arrow
  | IntInterval of string
